module.exports.serviceGET = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: "Service Get",
        input: event,
      },
      null,
      2
    ),
  };
};

/**
 *
 * @param {*} event Evento que se recibe
 * @param {*} context  Contexto de como debe de funcionar la lambda
 * @param {*} callback  Respuesta
 */
module.exports.servicePOST = async (event, context, callback) => {
  // indica si la funcion debe de terminar la ejecución una vez que ya se tiene una respuesta
  context.callbackWaitsForEmptyEventLoop = false;
  const body = JSON.parse(event.body);

  callback(null, {
    statusCode: 200,
    headers: {
      "Content-Type": "application/JSON",
    },
    body: JSON.stringify(
      {
        message: "Este es un post exitoso",
        body,
        success: true,
      },
      null,
      2
    ),
  });
};
