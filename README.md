# Api Serverless - AWS ses y rds

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/aws8512139/api-serverless-aws-ses-y-rds.git
git branch -M main
git push -uf origin main
```

## Accesos AWS

Recursos a los que debe de tener acceso (FullAccess) el usuario de AWS con el que se va trabajar son:

- RDS
- IAM
- SES
- Cloudwatch
- API Gateway
- CloudFormation

## Probar en local

- Para probar en local es neceario instalar el offline:

```
npm install serverless-offline --save-dev

```

- Levantar la api offline

```
sls offline start
```

## Desplegar los recursos

Para Desplegar los recursos se debe de ejecutar el siguiente comando:

```
sls deploy --verbose

sls deploy -f nombre_de_funcion_especifico
```

**--verbose** : Es para ver el paso a paso del despliegue

## Eliminar los recursos desplegados

Para eliminar los recursos desplegados se debe de ejecutar el siguiente comando:

```
sls remove
```

## Manejo de buenas practicas de código

- https://github.com/okonet/lint-staged
- husky
- eslint
- lint-staged

```
npx eslint --init

npx husky add .husky/pre-commit "npx lint-staged"
```

## Plugins

### serverless-plugin-include-dependencies

Permite no volver a subir el node_modules si ya se subio con anterioridad

```
npm install serverless-plugin-include-dependencies --save-dev
```
